function cambiarEstilo(){
	let titulo=document.getElementById("titulo");
	titulo.style.color="red";
	titulo.style.font="italic 100px georgia,san-serif";
}
function estiloOriginal(){
	let titulo=document.getElementById("titulo");
	titulo.style.color="white";
	titulo.style.font= "bold 100px Times New Roman, serif";
}
function tirar(ev){
	ev.preventDefault();
    let data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}
function dejaCaer(ev){
	ev.preventDefault();
}
function agarra(ev){
	ev.dataTransfer.setData("text", ev.target.id);
}
var repeticion=false;
function mensaje(){
	if(repeticion!=true){
		alert("Arrastra la imagen abajo del titulo, aproximadamente a\nla mitad de la pantalla =P");
		repeticion=true;
	}
}