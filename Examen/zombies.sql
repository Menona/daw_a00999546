-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-10-2017 a las 22:48:20
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `examen`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zombies`
--

CREATE TABLE `zombies` (
  `IdZombie` int(11) NOT NULL,
  `Nombre` varchar(15) DEFAULT NULL,
  `ApellidoP` varchar(15) DEFAULT NULL,
  `ApellidoM` varchar(15) DEFAULT NULL,
  `Tipo` varchar(15) DEFAULT NULL,
  `FechaRegistro` datetime DEFAULT NULL,
  `FechaTransicion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `zombies`
--

INSERT INTO `zombies` (`IdZombie`, `Nombre`, `ApellidoP`, `ApellidoM`, `Tipo`, `FechaRegistro`, `FechaTransicion`) VALUES
(1, 'Pepe', 'Perez', 'Juarez', 'Coma', '2017-10-05 00:00:00', '2017-10-15 00:00:00'),
(2, 'Agustin', 'Rosas', 'Fuentes', 'Infeccion', '2017-10-07 00:00:00', '2017-10-15 00:00:00'),
(3, 'Margarita', 'de la Rosa', 'Marquez', 'Coma', '2017-10-04 00:00:00', '2017-10-18 00:00:00'),
(4, 'Elhubiera', 'No', 'Existe', 'Muerto', '2017-09-20 00:00:00', '2017-10-18 00:00:00'),
(5, 'Akel', 'Rico', 'Madrazo', 'Transformacion', '2017-09-30 00:00:00', '2017-10-19 00:00:00'),
(6, 'wertqfd', 'sdfsdfs', 'dfsdas', 'Coma', NULL, NULL),
(7, 'wertg', 'esfdgf', 'dsfd', 'Coma', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'qertasdfg', 'sdffgb', 'dfgfg', 'Coma', '2017-10-27 10:14:40', '2017-10-27 10:14:40');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `zombies`
--
ALTER TABLE `zombies`
  ADD PRIMARY KEY (`IdZombie`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `zombies`
--
ALTER TABLE `zombies`
  MODIFY `IdZombie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
