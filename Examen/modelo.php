<?php
function conectDB(){
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "examen";
	
	$con = mysqli_connect($servername,$username,$password,$dbname);
	
	//CHECK CONNECTION
	if(!$con){
		die("Connection Failed: ". mysqli_connect_error());
	}
	$con->set_charset("utf8");
	return $con;
}

function closeDB($mysql){
	mysqli_close($mysql);
}

function agregarZombie($nombre,$apellidoP,$apellidoM,$estado,$fecha1,$fecha2){
	$conn = conectDB();
	$sql = 'INSERT INTO `zombies` (`Nombre`, `ApellidoP`, `ApellidoM`, `Tipo`,`FechaRegistro`, `FechaTransicion`) VALUES (?,?,?,?,?,?)';
	
	if(!($statement = $conn->prepare($sql))){
		die("Preparation failed");
	}
	if(!$statement->bind_param("ssssss", $nombre, $apellidoP, $apellidoM, $estado, $fecha1,$fecha2)){
		die("Parameter vinculation failed");
	}
	if(!$statement->execute()){
		die("Fall� la inserci�n");
	}
	closeDB($conn);
}

function borrarColaborador($IdColaborador){
	$conn = conectDB();
	$sql = 'DELETE FROM colaboradores WHERE IdColaborador = ?';
	$result=mysqli_query($conn,$sql);
    if (!($statement = $conn->prepare($sql))) {
        die("The preparation failed: (" . $conn->errno . ") " . $conn->error. '      Regresa y completa los valores correctamente');
    }
    // Binding statement params
    if (!$statement->bind_param("s", $IdColaborador)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error. '      Regresa y completa los valores correctamente');
    }
    // delete execution
    if ($statement->execute()) {
        echo 'There were ' . mysqli_affected_rows($conn) . ' affected rows';
    } else {
        die("Update failed: (" . $statement->errno . ") " . $statement->error. '      Regresa y completa los valores correctamente');
    }
	closeDB($conn);
	return $result;
}

function consultarDatos(){
	$conn = conectDB();
	$sql = "SELECT * FROM zombies";
	$result = mysqli_query($conn,$sql);

	echo "Cantidad de Zombies: ". mysqli_num_rows($result);
	$sql = "SELECT * FROM zombies WHERE Tipo = 'Coma'";
	$result = mysqli_query($conn,$sql);
	echo "<br>Zombies en Coma: ". mysqli_num_rows($result);
	$sql = "SELECT * FROM zombies WHERE Tipo = 'Muerto'";
	$result = mysqli_query($conn,$sql);
	echo "<br>Zombies Completamente Muertos: ". mysqli_num_rows($result);
	$sql = "SELECT * FROM zombies WHERE Tipo = 'Infeccion'";
	$result = mysqli_query($conn,$sql);
	echo "<br>Zombies Infectados: ". mysqli_num_rows($result);
	$sql = "SELECT * FROM zombies WHERE Tipo = 'Transformacion'";
	$result = mysqli_query($conn,$sql);
	echo "<br>Zombies en Transformacion: ". mysqli_num_rows($result);
	$sql = "SELECT Tipo FROM zombies WHERE Tipo = 'Coma' OR Tipo = 'Transformacion' OR Tipo = 'Infeccion'";
	$result = mysqli_query($conn,$sql);
	echo "<br>Zombies que no estan Completamente Muertos: ". mysqli_num_rows($result);
	closeDB($conn);
	return $result;
}

function imprimirZombie(){
	$conn = conectDB();
	$sql = "SELECT * FROM zombies";
	$result = mysqli_query($conn,$sql);
	if(mysqli_num_rows($result)>0){
		while($row = mysqli_fetch_assoc($result)){
			echo "<tr>";
			echo "<td>" . $row["Nombre"] . " </td>";
			echo "<td>" . $row["ApellidoP"] . " </td>";
			echo "<td>" . $row["ApellidoM"] . " </td>";
			echo "<td>" . $row["Tipo"] . " </td>";
			echo "<td>" . $row["FechaRegistro"] . " </td>";
			echo "<td>" . $row["FechaTransicion"] . " </td>";
			//echo '<td><i class="tiny material-icons">mode_edit</i></td>';
			/*echo '<td><a href="colaborador-eliminar.php?IdColaborador='.$row["IdColaborador"].'"><i class="tiny material-icons">delete</i></a></td>';
			*/echo "</tr>";
		}
	}
	
	mysqli_free_result($result);
	closeDB($conn);
	return $result;
}

function imprimirAlgunosZombie($estado){
	$conn = conectDB();
	$sql = "SELECT * FROM zombies WHERE Tipo LIKE '%".$estado."%'";
	$result = mysqli_query($conn,$sql);
	if(mysqli_num_rows($result)>0){
		while($row = mysqli_fetch_assoc($result)){
			echo "<tr>";
			echo "<td>" . $row["Nombre"] . " </td>";
			echo "<td>" . $row["ApellidoP"] . " </td>";
			echo "<td>" . $row["ApellidoM"] . " </td>";
			echo "<td>" . $row["Tipo"] . " </td>";
			echo "<td>" . $row["FechaRegistro"] . " </td>";
			echo "<td>" . $row["FechaTransicion"] . " </td>";
			//echo '<td><i class="tiny material-icons">mode_edit</i></td>';
			/*echo '<td><a href="colaborador-eliminar.php?IdColaborador='.$row["IdColaborador"].'"><i class="tiny material-icons">delete</i></a></td>';
			*/echo "</tr>";
		}
	}
	
	mysqli_free_result($result);
	closeDB($conn);
	return $result;
}
?>