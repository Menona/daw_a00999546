<?php
function conectDB(){
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "perros";
	
	$con = mysqli_connect($servername,$username,$password,$dbname);
	
	//CHECK CONNECTION
	if(!$con){
		die("Connection Failed: ". mysqli_connect_error());
	}

	return $con;
}

function closeDB($mysql){
	mysqli_close($mysql);
}

function guardarPerro($raza, $nombre, $peso, $comida) {
	$conn = conectDB();
	$sql = 'INSERT INTO `raza` (`Raza`, `Nombre`, `Peso`, `ComidaFav`) VALUES (?, ?, ?, ?)';
	
	if(!($statement = $conn->prepare($sql))){
		die("Preparation failed");
	}
	if(!$statement->bind_param("ssss", $raza, $nombre, $peso, $comida)){
		die("Parameter vinculation failed");
	}
	if(!$statement->execute()){
		die("Falló la inserción");
	}
	closeDB($conn);
}

function editarInfoPerro($id, $raza) {
	$conn = conectDB();
	$sql = "UPDATE raza SET Raza = '$raza' WHERE idPerro = $id";
	
	if(mysqli_query($conn,$sql)){
		echo "Records were updated sucessfully";
	}else{
		echo "ERROR FATAL!!! EL MUNDO EXPLOTARA POR TU CULPA!!!";
	}
	closeDB($conn);
}

function borrarPerro($id){
	$conn = conectDB();
	$sql = "DELETE FROM `raza` WHERE `raza`.`idPerro` = $id";
	$result=mysqli_query($conn, $sql);
	
	closeDB($conn);
	return $result;
}

function imprimirPerros(){
	$conn = conectDB();
	$sql = "SELECT idPerro, Raza, Nombre, Peso, ComidaFav FROM raza";
	$result = mysqli_query($conn, $sql);
	echo '<table class="centered bordered highlight"> 
	<thead>
			<tr>
				<th>ID</th>
				<th>Raza</th>
				<th>Nombre</th>
				<th>Peso(kg)</th>
				<th>Comida Favorita</th>
			</tr>
        </thead>
        <tbody>';
	if(mysqli_num_rows($result)>0){
		while($row = mysqli_fetch_assoc($result)){
			echo "<tr>";
			echo "<td>" . $row["idPerro"] . " </td>";
			echo "<td>" . $row["Raza"] . " </td>";
			echo "<td>" . $row["Nombre"] . " </td>";
			echo "<td>" . $row["Peso"] . " </td>";
			echo "<td>" . $row["ComidaFav"] . " </td>";
			echo "</tr>";
		}
	}
	echo "</table>";

	mysqli_free_result($result);
	closeDB($conn);
	return $result;
}

function getDogByName($dogname){
	$conn = conectDB();
	$sql = "SELECT idPerro, Raza, Nombre, Peso, ComidaFav FROM raza WHERE Nombre Like '%".$dogname."%'";
	$result = mysqli_query($conn, $sql);
	
	closeDB($conn);
	return $result;
}

function getFatDogs($fatdog){
	$conn = conectDB();
	$sql = "SELECT idPerro, Raza, Nombre, Peso, ComidaFav FROM raza WHERE Peso >= ".$fatdog;
	
	/*
	if(!($statement = $conn->prepare($sql))){
		die("Preparation failed");
	}
	if(!$statement->bind_param("s", $fatdog)){
		die("Parameter vinculation failed");
	}*/
	
	$result = mysqli_query($conn, $sql);
	$tabla='<table class="centered bordered highlight"> 
	<thead>
			<tr>
				<th>ID</th>
				<th>Raza</th>
				<th>Nombre</th>
				<th>Peso(kg)</th>
				<th>Comida Favorita</th>
			</tr>
        </thead>
        <tbody>';
	
	if(mysqli_num_rows($result)>0){
		while($row = mysqli_fetch_assoc($result)){
			$tabla .= "<tr>";
			$tabla .= "<td>" . $row["idPerro"] . " </td>";
			$tabla .= "<td>" . $row["Raza"] . " </td>";
			$tabla .= "<td>" . $row["Nombre"] . " </td>";
			$tabla .= "<td>" . $row["Peso"] . " </td>";
			$tabla .= "<td>" . $row["ComidaFav"] . " </td>";
			$tabla .= "</tr>";
		}
	}
	$tabla .= "</tbody></table>";
	$result = mysqli_query($conn, $sql);
	closeDB($conn);
	
	return $tabla;
}
?>